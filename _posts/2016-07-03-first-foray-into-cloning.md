---
title: first foray into cloning
authors:
- Mario
layout: post
stub: /blog/
categories: blog
tags: garden
cover: photo-1456973336295-46fc683d4276.jpeg
---

## introduction ##

Lately, I've really been getting into gardening. Even though I recently moved
into an apartment (this is temporary in nature, that will be discussed in the
future), I wanted to starting growing some herbs and vegetables (keep an eye out
for another post on my collection).

Specifically, for this post, I will walk you through my first attempt at trying
to clone multiple soft stem plants. This started when one of my coworkers (I'll
call him Robb[^1]) gave me a plant cloner. It's a simple device, consists of only a
couple of parts, most of which you can get from your local supermarket/grocery
store like Walmart.

## parts list ##

- [2" Net Cup][net cup]
- [2gal Plastic Pail][plastic pail]
- [2gal Gasket Lid][gasket lid]
- [2" Neoprene Net Cup Lids][net cup lids]
- [Aqua Culture Air Pump - 5-15gal Single Outlet][air pump]
- [Aquarium Tube Air Hose][air hose]
- [Aquarium Tube Air Stone][air stone]
- [2" Hole Saw - If you don't have one already][hole saw]

_Note: The links above are just examples, make sure to shop around to get the best
possible price. I also didn't individually vet each link/seller, so buy at your
own risk. It may be better if you go to a local hydroponics store to grab the
necessary items._

_Note 2: Some of these items come with more than you need, it's unfortunate, but
it does mean you can make more than one cloner, if you want!_

_Note 3: Since this post is written in [Markdown], links do not open
in a new window._

## making the cloner ##

Making the cloner is easy: using the hole saw, around the edge of the gasket lid,
cut 8 2" holes. Try to space them apart evenly, so that all the holes fit (though,
it doesn't have to be perfect). Once you are done, it should look something similar
to the image below (ignore the hose for now, I did say I got it completed haha).

![Plastic Pail with Gasket Lid and 8 3" Holes Cut Into][cloner holes cut]

Once you've got the holes cut, cut a small hole in the middle large enough to fit
the air hose that you purchased. Once that is done, thread the hose through the
hole, enough to make it to the bottom of the pail (take a look at the above image
if you are unsure of what to do).

Next, insert the air stone to the bottom of the hose (the end that's in the pail).

![Air Hose with Air Stone Inserted][air stone inserted]

You are now done with creating the cloner. Next steps will be to start cloning!

## using the cloner ##

Now that your cloner is ready to use, it's time to get 8 4" cuts of the plants
you want to clone. I will be using a variety of tomatoes, mint, and basil.
Make sure to cut from the top of the plant, on one of the (hopefully) many branches
it has (never from the main branch). Also, cut at a 45 degree angle. Take a look
at the couple of cuts I did below if you are not sure how to proceed.

![Overview of Cut Plants][cut plants]

![Close-up of Cut Plants][cut plants close]

Once you have your 8 cuts (it doesn't have to be 8, but, let's MAXIMIZE the use
of the cloner), strip the leaves at the bottom of the stem (only keep a couple
towards the top), and insert them into the net cup lids until they reach the
bottom of the net cup.

![Close-up of Cut Plants at the Bottom of the Net Cup][cut plants net cup bottom]

![All Cut Plants in Net Cups][all cut plants net cup]

Next, it's time to fill up the pail full of water! Put one of the net cups into
the pail, and fill it up full of water until the water is about 1 inch from
the bottom of the net cup.

![Pail Filled with Water][filled pail]

Now it's time to insert your net cups, connect the air hose to the air pump, plug
it into power, and place it near a window! We should start seeing roots grow from
our cuts in the next couple of weeks.

![Project Finished with all Cut Plants in Net Cups inside Pail, with Air Pump Connected][finished pail]

Stay tuned!

-Mario :)


UPDATE 2016-07-03--001: I incorrectly listed the size of the net cups, they are 2"
instead of 3".


---
[^1]: He didn't get me explicit permission to use his real name (though I didn't
    ask)


[//]: # "Reference Links"
[net cup]: https://www.amazon.com/NP2AB-Slotted-Hydroponics-Aquaponics-Orchids/dp/B00I1OCZLY/ "2" Net Cups"

[plastic pail]: https://www.amazon.com/Encore-Plastics-20256-Industrial-2-Gallon/dp/B00144EO62/ "2gal Plastic Pail"

[gasket lid]: https://www.amazon.com/Encore-Plastics-52250-White-2-Gallong/dp/B009YNYPCS/ "2gal Gasket Lid"

[net cup lids]: https://www.amazon.com/dp/B01AAXA6Y6/ "2" Neoprene Net Cup Lids"

[air pump]: https://www.amazon.com/Gallon-Single-Outlet-Aquarium-Check/dp/B01HNAOYS8/ "Aqua Culture Air Pump - 5-15gal Single Outlet"

[air hose]: https://www.amazon.com/Uxcell-Soft-Plastic-Oxygen-Black/dp/B00H4WZ3JA/ "Aquarium Tube Air Hose"

[air stone]: https://www.amazon.com/Yueton-Cylinder-Aquarium-Bubble-Aerator/dp/B01CQ9L9I4/ "Aquarium Tube Air Stone"

[hole saw]: http://www.lowes.com/pd/LENOX-2-in-Bi-Metal-Arbored-Hole-Saw/3361282 "2" Hole Saw"

[markdown]: http://daringfireball.net/projects/markdown/ "Markdown language reference"


[//]: # "Images"
{% assign asset = page.title | replace: " ", "-" %}
{% assign asset_loc = "/assets/images/posts/" | append: asset | append: "/" %}
[cloner holes cut]: {{ asset_loc | append: "Gasket Lid with Cutouts.jpg" | relative_url }} "Plastic Pail with Gasket Lid and 8 3" Holes Cut Into"

[air stone inserted]: {{ asset_loc | append: "Air Stone.jpg" | relative_url }} "Air Hose with Air Stone Inserted"

[cut plants]: {{ asset_loc | append: "Cut Plants.jpg" | relative_url }} "Overview of Cut Plants"

[cut plants close]: {{ asset_loc | append: "Cut Plants Close.jpg" | relative_url }} "Close-up of Cut Plants"

[cut plants net cup bottom]: {{ asset_loc | append: "Cut Plants Net Cup Bottom.jpg" | relative_url }} "Close-up of Cut Plants at the Bottom of the Net Cup"

[all cut plants net cup]: {{ asset_loc | append: "All Cut Plants In Net Cups.jpg" | relative_url }} "All Cut Plants in Net Cups"

[filled pail]: {{ asset_loc | append: "Pail Filled with Water.jpg" | relative_url }} "Pail Filled with Water"

[finished pail]: {{ asset_loc | append: "Project Finished.jpg" | relative_url }} "Project Finished with all Cut Plants in Net Cups inside Pail, with Air Pump Connected"
