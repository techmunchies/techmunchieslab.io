---
title: iPhone upgrade program and t-mobile
authors:
- Mario
layout: post
stub: /blog/
categories: blog
tags: development
assets:
cover: photo-1435575653489-b0873ec954e2.jpeg
---
If you are interested in signing up for Apple's iPhone Upgrade Program and have T-Mobile, I recommend you make sure that the phone you want is physically in stock in the Apple store, otherwise, you cannot join the program, as I found out personally. Additionally, neither Apple chat nor in-store staff are familiar with this policy either. I also checked the site, and didn't see any mention to this, aside from verbage stating to go to an Apple store  

So, be forewarned :)

-Mario :)
