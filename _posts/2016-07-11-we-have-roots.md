---
title: we have roots
authors:
- Mario
layout: post
stub: /blog/
categories: blog
tags: garden
assets: we-have-roots
cover: photo-1476400044994-709e2339ed9d.jpeg
---
So it's been 13 days since we first set up the plant cloner. Let's take a look.
At this point, you should start seeing little roots poking out from the cut stems.
If you do, great! That means the our plants are reaching towards the water, trying
to absorb as much water as possible. The leaves should still be green (though some
may be brown, that's OK). They're not ready yet for transplantation, but you are
one step closer!

![Plants with Baby Roots][baby roots]

Additionally, make sure the water level is not too low. The net cups should be
damp. If the net cups are not damp, check the water level, and ensure it's only
about 1" under the net cups.

Hopefully all is well, until next time!

-Mario :)

[//]: # "Images"
{% assign asset = page.title | replace: " ", "-" %}
{% assign asset_loc = "/assets/images/posts/" | append: asset | append: "/" %}
[baby roots]: {{ asset_loc | append: "Plant Roots.jpg" | relative_url }} "Plants with Baby Roots"
