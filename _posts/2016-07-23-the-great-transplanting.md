---
title: the great transplanting
authors:
- Mario
layout: post
stub: /blog/
categories: blog
tags: garden
cover: photo-1462663608395-404cb6246eaf.jpeg
---
Taking another look at our little clones, and wow, are the root systems long, strong, and got it goin' on!

![Nice Long Roots][roots]{: .portrait-800px}

Now that the plants are ready to be moved out of the cloner, I bought small, clear cups from my local grocery
store. Any will do, I got 9oz clear ones, so I can see the roots spread (and, let's be honest, it looks cool
as hell!). I then poked three holes in it, for drainage: one on each side, close to the bottom, and one on the
bottom of the cup, towards the middle.

![Cup with Holes in it][cup]{: .portrait-800px}

Fill the cup half way with soil (I used MiracleGro Potting Soil, mixed with Jobe's Organics Tomato & Vegetable
feed), holding the plant in place, distributing the roots well.

![Half Filled Cup with Plant][half cup]{: .portrait-800px}

Finish by topping off the cup full of soil, again taking care not to bury the leaves, and distributing the roots.
How spectacular. Wet the soil a little bit, put it on a window sill, or outside, and watch your little clones grow!

![Fully Filled Cup with Plant][full cup]{: .portrait-800px}

Until next time!

-Mario :)

[//]: # "Images
{% assign asset = page.title | replace: " ", "-" %}
{% assign asset_loc = "/assets/images/posts/" | append: asset | append: "/" %}
[roots]: {{ asset_loc | append: "Roots.jpg" | relative_url }} "Nice Long Roots"
[cup]: {{ asset_loc | append: "Cup.jpg" | relative_url }} "Nice Long Roots"
[half cup]: {{ asset_loc | append: "Half Filled.jpg" | relative_url }} "Half Filled Cup with Plant"
[full cup]: {{ asset_loc | append: "Full Filled.jpg" | relative_url }} "Full Filled Cup with Plant"
