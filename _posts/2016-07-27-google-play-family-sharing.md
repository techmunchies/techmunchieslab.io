---
title: google play family sharing
authors:
- Mario
layout: post
stub: /blog/
categories: blog
tags: tech
assets: google-play-family-sharing
cover: photo-1458592918806-99f9cfbe41fc.jpeg
---
Google Play Family Sharing is a thing. Now that it is, it's time to take advantage of it. One the more easier ways to sign
up for Family Sharing is through the Play Music app (that is, if you don't have a Google Play Music Family subscription
yet). Please follow the steps to enable Family Sharing (screenshots will be coming):

NOTE: You don't have to subscribe to do this trick. At the end, when it asks to subscribe, hit the back button, and exit
out. Only subscrible to Play Music Family if you want to.

1. Launch Play Music
2. Tap the hamburger menu <i class="fa fa-bars" aria-hidden="true"></i>
3. Tap Settings
4. Tap Subscribe
5. Tap Family
6. Tap Set Up Family
7. Tap Get Started
8. Tap Continue
9. Add or Accept your credit card (cannot take a screenshot due to restrictions set by Google)
10. Tap on Invite family members if you want to add more family members (skip to Step 16 if you don't)
11. Tap on one of the contacts show, or, Tap on Add Recipients (I manually added a recipient)
12. Type in the recipient's email, and accept. Continue adding recipient's until all six slots are taken, or until you've
    adding everying in your family
13. Tap Send
14. Tap Continue
15. Tap Continue
16. Either Tap the Back Button and exit Play Music, or Subscribe. Up to you. However, at this point, you've successfully
    created your family. If you decide to subscribe, you'll need to verify your card's CVC code.


I will be updating this post with pictures of the process, just wanted to get this out there in case anyone has any issues
creating their family.

Until next time!

-Mario :)

[//]: # "Images"
