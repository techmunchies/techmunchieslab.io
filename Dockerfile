FROM ruby:2.3-alpine
MAINTAINER mario@techmunchies.net

COPY Gemfile /

RUN \
  set -ex && \
  apk add --no-cache --virtual .build-deps \
    autoconf \
    bison \
    bzip2 \
    bzip2-dev \
    ca-certificates \
    coreutils \
    gcc \
    gdbm-dev \
    glib-dev \
    libc-dev \
    libffi-dev \
    libxml2-dev \
    libxslt-dev \
    linux-headers \
    make \
    ncurses-dev \
    openssl \
    openssl-dev \
    procps \
    readline-dev \
    tar \
    yaml-dev \
    zlib-dev && \
  bundle install --path vendor --without htmlproofer && \
  apk del .build-deps

VOLUME /srv
EXPOSE 4000

WORKDIR /srv
CMD ["bundle exec jekyll serve --drafts --config _config.yml,_config_docker.yml -H 0.0.0.0"]
