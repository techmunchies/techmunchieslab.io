// Check position of scroll, and if it's greater than 80px, add the scrolled
// class to the nav
$(() => $(window).on('scroll', () => {
  const yPos = $(window).scrollTop();

  if (yPos >= 80) {
      $('.header').addClass('scrolled');
  }
  else {
      $('.header').removeClass('scrolled');
  }
}));

$(() => $(window).on('scroll', () => {
  var yPos = $(window).scrollTop();

  if (yPos >= 150) {
    $('.to-top').removeClass('hidden');
  }
  else {
    $('.to-top').addClass('hidden');
  }
}));

// Execute when clicking on the back to top button
$(".to-top").on('click', () => {
  $("html, body").animate({scrollTop: 0}, 1000);
});

// Checks to see if code editor div exists
if ($('#code-editor').length > 0) {
  // Sets up code editor on main site
  const codeEditor = ace.edit('code-editor');

  codeEditor.session.setOptions({
    fontFamily: 'source-code-pro',
    mode: 'ace/mode/html',
    tabSize: 2,
    useSoftTabs: true
  });

  codeEditor.setTheme('ace/theme/solarized_dark');
  codeEditor.setReadOnly(true);
  codeEditor.setValue(
    '<!DOCTYPE html>\n' +
    '<html lang="en">\n' +
    '<head>\n' +
    '  <title>my awesome site!</title>\n' +
    '  <meta name="description" content="site -- it\'s awesome!">\n' +
    '  <meta charset="utf-8">\n' +
    '  <meta http-equiv="X-UA-Compatible" content="IE=edge">\n' +
    '  <meta name="viewport" content="width=device-width, initial-scale=1">\n' +
    '  <link rel="shortcut icon" href="favicon.png" />\n' +
    '  <link rel="canonical" href="http://site.com/">\n' +
    '\n' +
    '  <!-- Custom CSS -->\n' +
    '  <link rel="stylesheet" href="main.css">\n' +
    '</head>\n' +
    '<body>\n' +
    '  <div class="nav">\n' +
    '    <div class="nav-menu">\n' +
    '      <ul>\n' +
    '        <li>\n' +
    '          <a href="/" title="site home">Home</a>\n' +
    '        </li>\n' +
    '        <li>\n' +
    '          <a href="/gallery" title="site gallery">Gallery</a>\n' +
    '        </li>\n' +
    '      </ul>\n' +
    '    </div>\n' +
    '  </div>\n' +
    '\n' +
    '  <div class="jumbo">\n' +
    '    <h1 class="title">My Awesome Site</h1>\n' +
    '    <h1 class="intro">\n' +
    '      Where awesomeness comes easy :)\n' +
    '    </h1>\n' +
    '  </div>\n' +
    '\n' +
    '  <!-- Custom JS -->\n' +
    '  <script src="main.js"></script>\n' +
    '</body>\n' +
    '</html>');
  codeEditor.setAutoScrollEditorIntoView(false);
  codeEditor.getSession().selection.clearSelection();
  codeEditor.$blockScrolling = Infinity;
}

// Binds to the submit action of the contact form
$('#contact-form').submit((e) => {

  // Let's select and cache all the fields
  const $inputs = $('#contact-form').find('input, select, button, textarea');

  // Submit button
  const $submit = $('#submit');

  // Submit button text
	const submitText = $submit.val();

  // Builds the data values from form
  const name     = $('#name').val();
  const email    = $('#email').val();
  const message  = $('#message').val();

  // Let's disable the inputs for the duration of the AJAX request.
  $inputs.prop('disabled', true);

  // Send AJAX request
  $.ajax({
    url: 'https://formspree.io/mario@techmunchies.net',
    method: 'POST',
    data: {
      name:name,
      _replyto:email,
       email:email,
      message:message,
      _subject:'techmunchies form submission'
    },
    dataType: 'json',

    beforeSend: () => {
      $submit.val('Sending Message');
    },

    success: (response) => {
      $submit.val('Message Sent!');
      setTimeout(() => {
        $submit.val(submitText);

        // Reenable the inputs
        $inputs.prop('disabled', false);

        // Reset form
        $('#contact-form').trigger('reset');
        $('#contact-form input[type="text"], input[type="email"]').trigger('click');
      }, 5000);
    },

    error: () => {
      $submit.val('There was an error.');
      setTimeout(() => {
        $submit.val(submitText);

        // Reenable the inputs
        $inputs.prop('disabled', false);

        // Reset form
        $('#contact-form').trigger('reset');
        $('#contact-form input[type="text"], input[type="email"]').trigger('click');
      }, 5000);
    }
  });

  // Prevent default posting of form
  e.preventDefault();
});
