# techmunchies.net

[![build status](https://gitlab.com/techmunchies/techmunchies.gitlab.io/badges/master/build.svg)](https://gitlab.com/techmunchies/techmunchies.gitlab.io/commits/master) [![Join the chat at https://gitter.im/mariolopjr/mariolopjr.github.io](https://badges.gitter.im/mariolopjr/mariolopjr.github.io.svg)](https://gitter.im/mariolopjr/mariolopjr.github.io?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)

# running locally
So...you want to build and run the site locally huh? Interested in my Docker setup, which simplifies portability???  
Well, you've come to the right place!

Please, ensure you have Docker installed! Then, `cd` to the directory that you cloned this repo to, and run the following command:  

```bash
docker run --rm -it -v "$PWD:/srv" -p 80:4000 ruby:2.3 "/bin/bash"
```

This will bind to the your local port 80 yay! Now you will be _thrust_ into a bash shell within the Docker container...cool!  

Next,

```bash
bundle install --path vendor --without htmlproofer
```

```bash
bundle exec jekyll serve --config _config.yml,_config_docker.yml -H 0.0.0.0
```
